name:    ping_server module
license: GPL
by:      Breyten Ernsting (bje@dds.nl)
requirements: Drupal 4.6.3 or above.

1. Copy the files to a directory of your choice (say, site/modules/ping_server)
2. execute the statements in the ping_server.mysql file in phpmyadmin, or via the command line.
3. Activate the module in admin/modules
4. Go the settings page for this module: admin/settings/ping_server
5. adjust the settings.
6. enjoy!